Sutty Pocketbase Playbook
-------------------------

Pocketbase Ansible playbook for Sutty.  Allows to run several Pocketbase
instances per host.

This playbook assumes you have a Certbot container running, so it can
issue certificates, and an Nginx container to reverse proxy.

Also a DNS record for each `pocketbase.SITE` should exist and point to
the appropiate hosts.

Inventory
---------

Create, link or clone an `inventory.yml` file with the host roles.

```yaml
---
sutty_pocketbase:
  hosts:
    host.name:
    host2.name:
sutty_certbot:
  hosts:
    host.name:
```

Host vars
---------

Create, link or clone a directory `host_vars/` with a YAML file for each
host, containing all host-specific variables.

On `host_vars/host.name.yml`:

```yaml
---
pocketbases:
- name: "pocketbase"
  site: "sutty.nl"
  secret: "11111111111111111111111111111111"
  version: "3.18.5"
```

Migrations
----------

To copy migrations from a project to a Pocketbase container, copy or
symlink them into `_data/NAME/migrations`.

Run
---

Make sure everything's correctly configured.  It'll probably fail to
check the existence of some files on first run.

```sh
ansible-playbook -i inventory.yml playbook.yml -C
```

Remove the `-C` flag to actually run the playbook.

Configuration
-------------

Visit `https://NAME.SITE/_/` to visit the Pocketbase administration
panel and start configuration.
